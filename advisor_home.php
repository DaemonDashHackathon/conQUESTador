<?php
require_once 'CAS/config.php';
require_once 'CAS/CAS.php';
require_once 'CAS/init.php';
?>
<?php if( ! phpCAS::isAuthenticated() ) {
header("Location: index.php");
}
//check if advisor, redirect to appropriate home page?
?>
<?php include("php/funcs.inc.php"); ?>
<!DOCTYPE html>
<html>
<?php addHeader("TerPlan: Home"); ?>
<link href="css/justified-nav.css" rel="stylesheet">
<link href="js/jqplot/jquery.jqplot.css" rel="stylesheet">
</head>
<body style="padding-top:0px;">

<?php makeNav("home"); ?>

<div class="container">
  <div class="jumbotron">
    <h1>TerPlan</h1>
    <p class="lead">Management of student data made easy! See below to get started.</p>
  </div>
  <div class="row">
    <div class="col-lg-4">
      <h2>Class Statistics</h2>
      <p class="lead">There are <strong>23</strong> open seats in 4XX level courses.</p>
      <div style="height: 250px;width: 375px;" id="chartdiv"></div>
      <p><a class="btn btn-primary" href="stats.php">More Details &raquo;</a></p>
    </div>
    <div class="col-lg-4">
      <h2>Meeting with Students?</h2>
      <p class="lead">Show them how easy it is to understand their schedule.</p>
      <p><a class="btn btn-primary" href="#">Lets Go &raquo;</a></p>
    </div>
    <div class="col-lg-4">
      <h2>Advising Progress</h2>
<?php $per = perComp(); ?>
<p class="lead">You've already met with <strong>68%</strong> of your students!</p>
      <div class="progress progress-striped">
      <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%"><span class="sr-only">68% Complete</span></div>
      </div>
      <p><a class="btn btn-primary" href="#">Schedule Meetings &raquo;</a></p>
    </div>
</div>

<?php addJS(); ?>
<script type="text/javascript" src="js/jqplot/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="js/jqplot/plugins/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="js/jqplot/plugins/jqplot.trendline.min.js"></script>
<script>
$(document).ready(function() {
  $.jqplot.config.enablePlugins = true;
  $.jqplot('chartdiv', [[[1,10], [2, 15], [3, 23], [4, 28], [5, 25], [6, 30]]], {
    animate: true,
    animateReplot: true,
    axes: {
      xaxis: {
        showTicks: false,
        numberTicks: <?php echo sizeof(semesterGPAs())/2; ?>,
      },
      yaxis: {
        showTicks: true,
      },
    },
    highlighter: { show: true, }
  });
});
</script>
</body>
</html>
