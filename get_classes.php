<?php

$mysqli = new mysqli("54.218.151.84", "root", "testudo", "hardshell");
if( $mysqli->connect_errno) {
  echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}  
// if( !isset($_POST['query']) ) { $_POST['query'] = 'CMSC'; }
if( !isset($_POST['tags']) ) { return; }
$stmt = $mysqli->prepare("SELECT coursecode FROM `student_classes` WHERE username = ?");
$param = $_POST['tags'];
$stmt->bind_param('s', $param);
$stmt->execute();
$stmt->bind_result($name);

$list = Array();
while($stmt->fetch() ) {
  $list[] = $name;
}

echo json_encode($list);
?>
