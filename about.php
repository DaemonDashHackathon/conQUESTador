<?php include("php/funcs.inc.php"); ?>
<!DOCTYPE html>
<html>
<?php addHeader("TerPlan: About"); ?>
<link href="css/jumbotron-narrow.css" rel="stylesheet">
</head>
<body>

<div class="container">
  <div class="header">
    <ul class="nav nav-pills pull-right">
      <li><a href="home.php">Home</a></li>
      <li class="active"><a href="#">About</a></li>
    </ul>
    <h3 class="text-muted">TerPlan</h3>
  </div>
  
  <div class="row">
    <div class="col-lg-12">
      <p>Schedshell was created by a team of <a href="http://quest.umd.edu">Quest</a> students at UMD's <a href="http://daemondash.cs.umd.edu">Daemon Dash Hackathon</a>.</p>
    </div>
  </div>
  <div class="row marketing">
    <div class="col-lg-6">
      <h4>Eric Zinnikas</h4>
      <img src="img/eric-headshot.jpg" alt="Eric Zinnikas" class="img-thumbnail">
      <p>Eric is a Junior studying Computer Science.</p>

      <h4>Paige Nelson</h4>
      <img src="img/paige-headshot.jpg" alt="Paige Nelson" class="img-thumbnail">
      <p>Paige is a Junior studying Computer Science.</p>
    </div>
    <div class="col-lg-6">
      <h4>Danny Laurence</h4>
      <img src="img/danny-headshot.jpg" alt="Danny Laurnce" class="img-thumbnail">
      <p>Danny is a Senior studying Computer Science.</p>

      <h4>Raja Ayyagari</h4>
      <img src="img/raja-headshot.jpg" alt="Raja Ayyagari" class="img-thumbnail">
      <p>Raja is a Junior studying Computer Science.</p>
    </div>

    <div class="footer">
      <p>&copy; UMD 2013</p>
    </div>
</div>


<?php addJS(); ?>
</body>
</html>
