<?php
require_once 'CAS/config.php';
require_once 'CAS/CAS.php';
require_once 'CAS/init.php';
?>
<?php if( ! phpCAS::isAuthenticated() ) {
header("Location: index.php");
}
?>
<?php if( isset($_POST['transcript']) ) {
  $temp = tempnam(sys_get_temp_dir(), "transcript");
  file_put_contents($temp, $_POST['transcript']);
  $outstatus = Array();
  exec("/usr/bin/ruby1.9.1 rb/parseTranscript.rb ".$temp." ".phpCas::getUser(), $outstatus);
  if( sizeof($outstatus) > 0 && $outstatus[0] == "Success") { 
    header('Location: home.php?success=');
  }
}
?>
<?php include("php/funcs.inc.php"); ?>
<!DOCTYPE html>
<html>
<?php addHeader("TerPlan: Upload Transcript"); ?>
</head>
<body>

<?php makeNav("student_upload"); ?>

<div class="container">
  <div class="row">
<?php if( isset($_POST['transcript']) ) { ?>
<?php if(sizeof($outstatus) > 0 && $outstatus[0] == "Success") {
?>
<div class="col-lg-offset4 col-lg-4">
  <div class="alert alert-success"><strong>Success!</strong> Transcript Imported.</div>
<script>
</script> 
</div>
<?php
} else {
?>
<div class="col-lg-offset4 col-lg-6">
  <div class="alert alert-danger"><strong>Error!</strong> Transcript was not successfully parsed.  Try again <a href="upload.php">here</a>.</div>
</div>
<?php
}
?>
<?php } else { ?>
    <div class="col-lg-6">
      <ol>
        <br />
        <li class="lead">View your transcript <a href="https://www.sis.umd.edu/testudo/uoTrans" target="_blank">here</a>.</li>
        <li class="lead">Copy the entire web page (Ctrl-A; Ctrl-C).</li>
        <li class="lead">Paste it in this box! (Ctrl-V).</li>
        <li class="lead">Hit Submit!</li>
      </ol>
    </div>
    <div class="col-lg-3">
      <form role="form" action="" method="POST">
        <div class="form-group">
          <label><p class="lead">Paste Transcript Here:</p></label><br />
          <textarea style="width: 100%;" name="transcript" rows="3"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
<?php } ?>
  </div>
</div>

<?php addJS(); ?>
</body>
</html>
