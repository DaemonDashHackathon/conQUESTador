<?php
//cas urls:
// login: https://login.umd.edu/cas/login
// logout
// validate
// serviceValidate
// proxy
// proxyValidate

phpCAS::setDebug(); //debug ON
phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context);

//phpCAS::setCasServerCACert($cas_server_ca_cert_path);

phpCAS::setNoCasServerValidation();
?>
