<?php
//cas urls:
// login: https://login.umd.edu/cas/login
// logout
// validate
// serviceValidate
// proxy
// proxyValidate

require_once 'config.php';
require_once $phpcas_path . '/CAS.php';
include 'init.php';

phpCAS::forceAuthentication();

if( isset($_REQUEST['logout']) ) {
  //UMD cas doesn't let you redirect on logout :(
  phpCAS::logout();
}
//header("Location: http://54.218.151.84:8080/final/conQUESTador/home.php");
header("Location: http://www.terplan.me/home.php");
exit;
?>
