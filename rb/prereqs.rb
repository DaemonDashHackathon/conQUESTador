require 'open-uri'
require 'mysql'

my = Mysql::new('54.218.151.84', 'root', 'testudo', 'hardshell'); #censored

departments = Array.new

deps = open('https://ntst.umd.edu/soc/').read
deps.scan(/<span class="prefix-abbrev push_one two columns">(\w{4})<\/span>\s+<span class="prefix-name nine columns">(.+?)<\/span>/) { |abbr,full|
  departments << abbr
  my.query("INSERT IGNORE INTO testudo_departments(abbr, name) VALUES('" + abbr + "', '" + Mysql.escape_string(full) + "')")
}

dep_count = 0
course_count = 0
semester = "201308"

departments.each { |abbr|
  dep_count = dep_count + 1

  page = open('https://ntst.umd.edu/soc/all-courses-search.html?course=' + abbr + '&term=' + semester).read

  page.scan(/<div id="(\w{4}\d{3}\w?)" class="course">(.+?)\s+<\/div>\s+<\/div>\s+<\/div>\s+<\/div>\s+<\/div>\s+<\/div>/m) { |id,info|
    course_count = course_count + 1
    credits = info.scan(/<span class="course-min-credits">(\d)<\/span>/).join("")
    
    my.query("INSERT IGNORE INTO testudo_classes(coursecode, credits, department, semester_id) VALUES('" + id + "', '" + credits + "', '" + abbr + "', '" + semester + "')")
    my.commit # not sure if needed
  
    info.scan(/<div class="approved-course-text">Prerequisite:(.+?)\./) { |text| #text is an array for some reason?? not a string??
      my.query("UPDATE testudo_classes SET prereqs='" + text[0].scan(/\w{4}\d{3}\w?/).join(", ") + "' WHERE coursecode='" + id + "'")
    }

    info.scan(/<div class="approved-course-text">(?:Prerequisite:.+?)*Corequisite:(.+?)\./) { |text| #text is an array for some reason?? not a string??
      my.query("UPDATE testudo_classes SET coreqs='" + text[0].scan(/\w{4}\d{3}\w?/).join(", ") + "' WHERE coursecode='" + id + "'")
    }

    info.scan(/(?:<div class="approved-course-text">(?:Prerequisite|Corequisite|Restriction|Also).+?)*<div class="approved-course-text">(.+?)<\/div>/m) { |desc| #same issue as above...1 element array?
      my.query("UPDATE testudo_classes SET description='" + Mysql.escape_string( desc[0] ) + "' WHERE coursecode='" + id + "'")
    }
  }
}

puts "Added #{course_count} courses in #{dep_count} departments."
