#extract: degree, semester info (semester name, coursecode, course name, credits, QP; semester gpa; cumulative gpa)
require 'mysql'

filename = ARGV[0]
username = ARGV[1]
mysqlHost = "54.218.151.84"
mysqlUser = "root"
mysqlPass = "testudo"
mysqlDB = "hardshell"

db = Mysql::new(mysqlHost, mysqlUser, mysqlPass, mysqlDB)

lastSem = ""
semRegex = /^((Fall|Spring|Winter|Summer) 20[0-9]{2})/
degRegex = /^(.+?)\s\s/
classRegex = /^\s*([A-Z]*[0-9]*[A-Z]?)\s(.+?)\s*([A-Z][-|+]?)\s*([0-9]\.[0-9]{2})\s*([0-9]\.[0-9]{2})\s*([0-9]{1,2}\.[0-9]{2})\s*(([A-Z]{1,2}\s*)*)$/

transcript = IO.readlines(filename)
if( transcript.length < 5 )
	puts "Error"
	exit
end

i = 0
for i in 0..100
  if (/E-Mail/=~transcript[i])
      break
  end
end 
degRegex =~ transcript[i+3]
degree = $1
degree = "Computer Science"
db.query("INSERT INTO students(username, degree_id) VALUES('" + username + "', (SELECT degree_id FROM degree WHERE degree_name = '" + degree + "'))")

transcript.each { |line| 
  if (semRegex =~ line)
    year = $1[-4,4]
    case $1[0..($1.length - 6)]
    when "Spring"
      term = "01"
    when "Summer"
      term = "05"
    when "Fall"
      term = "08"
    when "Winter"
      term = "12"
    end
    lastSem = year + term
  elsif (classRegex =~ line)
    code = $1
    name = $2
    grade = $3
    credits = $5
    qp = $6
    if( $7 )
      corecodes = $7.split
    end

    db.query("INSERT INTO student_classes(coursecode, semestername, username, coursename, coursegrade, courseqp, coursecredits, corecode) VALUES('" + code + "', '" + lastSem + "', '" + username + "', '" + name + "', '" + grade + "', '" + qp + "', '" + credits + "', '" + corecodes.join(",") + "')")
  end
}

puts "Success"
