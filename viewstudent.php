<?php
require_once 'CAS/config.php';
require_once 'CAS/CAS.php';
require_once 'CAS/init.php';
?>
<?php if( ! phpCAS::isAuthenticated() ) {
header("Location: index.php");
}
?>
<?php include("php/funcs.inc.php"); ?>
<!DOCTYPE html>
<html>
<?php addHeader("TerPlan: Advisor View Student Transcript"); ?>
<link href="css/jquery-ui.css" rel="stylesheet">
</head>
<body>

<?php
	makeNav("adivsor_student");
?>

<div class="container">
  <div class="row">
		<?php 

			$user = phpCAS::getUser();

			$mysqli1 = new mysqli("54.218.151.84", "root", "testudo", "hardshell");
			if( $mysqli1->connect_errno) {
    				echo "Failed to connect to MySQL: (" . $mysqli1->connect_errno . ") " . $mysqli1->connect_error;
  			}			
				
			if($stmt1 = $mysqli1->prepare("SELECT degree FROM `advisor` WHERE `advisor`.`username` = ?")){
				$stmt1->bind_param('s',$user);
				$stmt1->execute();
				$stmt1->bind_result($degree_id);
				$stmt1->fetch();
				
				if($degree_id != NULL){
					$cool = true;
				}else{
					$cool = false; 
				}
			
			}

			if($cool){
			echo "<div class=\"col-lg-3\">";
  				echo "<form id=\"student_search\">";
				echo "<label for=\"tags\">Student's Name: </label>";
  				echo "<input id=\"tags\" name=\"tags\" />";
				echo "</form>";
			echo "</div>";
			
			$mysqli = new mysqli("54.218.151.84", "root", "testudo", "hardshell");
			if( $mysqli->connect_errno) {
    				echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
  			}	
				
			if($stmt = $mysqli->prepare("SELECT * FROM `degree`,`group` WHERE `degree`.`degree_id` = ? AND `degree`.`degree_id` = `group`.`degree_id` AND NOT `group`.`order` = 0")){
					
				$stmt->bind_param('i',$degree_id);
					
				if($stmt->execute()){
					
					$stmt->bind_result($degree_id,$degree_name,$group_id,$trash,$group_Name,$order);
					
					echo "<div id=\"accordion\" class=\"col-lg-7\">";
					while($stmt->fetch()){
						echo "<h3>" . $group_Name . "</h3>";
						echo "<div>";
						printGroup($group_id);
						echo "</div>";
					}
					echo "</div>";
				} else {
					echo $stmt->error;
				}

			} else {
				echo $mysqli->error;
				echo "failed";
			}
			} else {

				echo "Not cool, bro";
			}
		
		function printGroup($groupID){
			$mysqli = new mysqli("54.218.151.84","root","testudo","hardshell");
			if( $mysqli->connect_errno) {
				echo "Falied to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
			}

			

			if($stmt = $mysqli->prepare("SELECT type,data FROM `group_entry` WHERE `group_entry`.`group_ID` = ? ORDER BY `group_entry`.`type`,`group_entry`.`order`")){
					
				$stmt->bind_param('i',$groupID);
						
				if($stmt->execute()){
					
					$stmt->bind_result($type,$data);
					$groups = false; 
					while($stmt->fetch()){
						if($type == "class" && $data != ""){
							echo "<p>" . $data . "</p>";
						} else {
							if($groups == false){
								$mysqli2 = new mysqli("54.218.151.84","root","testudo","hardshell");
								if( $mysqli2->connect_errno) {
									echo "Falied to connect to MySQL: (" . $mysqli2->connect_errno . ") " . $mysqli2->connect_error;
								}

			
								if($stmt2 = $mysqli2->prepare("SELECT group_Name FROM `group` WHERE `group`.`group_ID` = ?")){
									$stmt2->bind_param('i',$data);
									$stmt2->execute();
									$stmt2->bind_result($group_name);
									$stmt2->fetch();
									echo "<div id=\"accordion\" class\"col-lg-7\">";
									echo "<h3>" . $group_name . "</h3>";
									echo "<div>";
									printGroup($data);
									echo "</div>";
								}							
								$groups = true; 
							} else {
								$mysqli1 = new mysqli("54.218.151.84","root","testudo","hardshell");
								if( $mysqli1->connect_errno) {
									echo "Falied to connect to MySQL: (" . $mysqli1->connect_errno . ") " . $mysqli1->connect_error;
								}

			
								if($stmt1 = $mysqli1->prepare("SELECT group_Name FROM `group` WHERE `group`.`group_ID` = ?")){
									$stmt1->bind_param('i',$data);
									$stmt1->execute();
									$stmt1->bind_result($group_name);
									$stmt1->fetch();
									echo "<h3>" . $group_name . "</h3>";
									echo "<div>";
									printGroup($data);
									echo "</div>";
								}
							}
						}
					}
					if($groups == true)
						echo "</div>";
				} else {
					echo $stmt->error;
				}

			} else {
				echo $mysqli->error;
				echo "failed";
			}

		

		}	
		?>
        </div>

  </div>

  
  </div>
</div>

<?php addJS(); ?>
	<script>
	$( document ).ready(function() {

		var availableTags = [
		<?php 
			$mysqlis = new mysqli("54.218.151.84", "root", "testudo", "hardshell");
			if( $mysqlis->connect_errno) {
    				echo "Failed to connect to MySQL: (" . $mysqlis->connect_errno . ") " . $mysqlis->connect_error;
  			}			
				
			if($stmts = $mysqlis->prepare("SELECT username FROM `students`")){
				$stmts->execute();
				$stmts->bind_result($username);
				
				$strbuild = "";
				while($stmts->fetch()){
					$strbuild = $strbuild . "\"" . $username . "\",";
				}
				echo substr_replace($strbuild ,"",-1);
				
			}


		?>
		];

    	$( "#tags" ).autocomplete({
      			source: availableTags
    		});

    	$("#accordion").accordion({
			collapsible:true,
			active:false,
			heightStyle:"content",
			beforeActivate: function(event, ui) {
         		// The accordion believes a panel is being opened
         		if (ui.newHeader[0]) {
         			var currHeader  = ui.newHeader;
         			var currContent = currHeader.next('.ui-accordion-content');
         		// The accordion believes a panel is being closed
     			} else {
     				var currHeader  = ui.oldHeader;
     				var currContent = currHeader.next('.ui-accordion-content');
     			}
         		// Since we've changed the default behavior, this detects the actual status
         		var isPanelSelected = currHeader.attr('aria-selected') == 'true';

         		// Toggle the panel's header
         		currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));

        		// Toggle the panel's icon
        		currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e',isPanelSelected).toggleClass('ui-icon-triangle-1-s',!isPanelSelected);

         		// Toggle the panel's content
         		currContent.toggleClass('accordion-content-active',!isPanelSelected)    
         		if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }

        		return false; // Cancel the default action
    			}

		});

		function validateChild(json,html,header,countToValidate){
			var count = 0;
			console.log($(html));
			for(j = 0; j < countToValidate; j++){
				var currChild = $(html).children()[j];
				console.log($(currChild));
				if($(currChild)[0].nodeName == "P"){
					$(currChild)[0].innerHTML = $(currChild)[0].innerText;
					
					if (contains(json,$(currChild)[0].innerText)){
						$(currChild)[0].innerHTML = "<b>" + $(currChild)[0].innerText + "</b>";
						count++;
					}
				} else {
					console.log($(currChild).children().length);
					var topTotal2 = $(currChild).children().length / 2;
					for(h = 0; h < topTotal2; h++){
						var currChild2 =  $(currChild).children()[2*h+1];
						console.log($(currChild2));
						var countToValidate2 = $(currChild2).children().length;
						console.log(countToValidate2);
						validateChild(json,currChild2,$(currChild).children()[2*h],countToValidate2);
					}
				}
			}
			if(count == countToValidate){
				//alert("Valid");
				$(header).css("background", '#15a589');
				$(header).css("color", 'white');
			} else if(count == 0) {
				//alert("Not Started");
				$(header).css("background", '#868686');
				$(header).css("color", 'white');
			} else {
				//alert("Mixed");
				$(header).css("background", '#82bbb0');
				$(header).css("color", 'white');
			}
		}

		function contains(json,toFind){
			for(i = 0; i < json.length; i++){
				if(json[i] == toFind){
					return true;
				}
			}
			return false;
		}
			

		$("#student_search").submit(function(){
			event.preventDefault();
			$.ajax({
				url: "get_classes.php",
				type: "post",
				data: $("#student_search").serialize(),
				// callback handler that will be called on success
				success: function(response, textStatus, jqXHR){
					var json = $.parseJSON(response);
					var topTotal = $("#accordion").children().length / 2;
					for(k = 0; k < topTotal; k++){
						var currChild1 =  $("#accordion").children()[2*k+1];
						var countToValidate1 = $(currChild1).children().length;
						validateChild(json,currChild1,$("#accordion").children()[2*k],countToValidate1);
					}
					
				},
				error: function(response, textStatus, jqXHR){
					var json = $.parseJSON(data);
       					alert(json.error);
				}
			});
		});		
	});
 	</script>
</body>
</html>
