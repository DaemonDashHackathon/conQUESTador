<?php 
require_once 'CAS/config.php';
require_once 'CAS/CAS.php';
require_once 'CAS/init.php';
?>


<?php include("php/funcs.inc.php"); ?>
<!DOCTYPE html>
<html>
<?php addHeader("TerPlan: Index"); ?>
<link href="css/jumbotron-narrow.css" rel="stylesheet">
</head>
<body>

<div class="container">
  <div class="header">
    <ul class="nav nav-pills pull-right">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="about.php">About</a></li>
    </ul>
    <h3 class="text-muted">TerPlan</h3>
  </div>
  
  <div class="jumbotron">
    <h2>Degree Navigation Made Easy</h2>
    <p class="lead">With our innovative approach to managing your courses, degree completion and tracking your GPA, planning for your future has never been easier.</p>
    <img class="img-rounded" style="position:relative; left: -14px; padding-bottom: 20px;" src="img/big.png" />
    <p><a class="btn btn-lg btn-success" href="CAS/auth.php">Sign In</a></p>
  </div>

  <div class="row marketing">
    <div class="col-lg-6">
      <h4>For Students</h4>
      <p>Easily track degree completion requirements and GPA.</p>
    </div>
    <div class="col-lg-6">
      <h4>For Advisors</h4>
      <p>Help your students visualize their degree and better plan their semester.</p>
    </div>

    <div class="footer">
      <p>&copy; UMD 2013</p>
    </div>
</div>


<?php addJS(); ?>
</body>
</html>
