<?php

function addNav($page) {

  $degree = "";
  $upload = "";
  $gpa = "";

  if($page == "degree") {
    $degree = "class='active'";
  } else if($page == "upload") {
    $upload = "class='active'";
  } else if($page == "gpa") {
    $gpa = "class='active'";
  }

  $nav = <<< NAV
<div class="navbar navbar-default navbar-static-top">
   <div class="container">
     <div class="navbar-header">
       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
       </button>
       <a class="navbar-brand" href="home.php"><img class="img-rounded" style="margin: 0px; padding: 0px; padding-right: 10px;" width="35px;" height="25px;" src="img/brand.png" />TerPlan</a>
     </div>
     <div class="navbar-collapse collapse">
       <ul class="nav navbar-nav">
         <li $degree><a href="viewstudent.php">Manage Degree</a></li>
         <li $upload><a href="upload.php">Upload Transcript</a></li>
         <li class="dropdown" $gpa>
           <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tools <b class="caret"></b></a>
           <ul class="dropdown-menu">
             <li $gpa><a href="stats.php">GPA Graph</a></li>
           </ul>
         </li>
       </ul>
       <div class="nav navbar-nav navbar-right">
       <ul class="nav navbar-nav">
         <li><a href="home.php">Home</a></li>
         <li><a href="CAS/auth.php?logout=">Logout</a></li>
       </ul>
       </div>
       </form>
     </div><!-- end nav-collapse-->
   </div>
</div>
NAV;
  echo $nav;
}

function addHeader($page) {
  $header = <<< HEAD
<head>
  <title>$page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="css/bootstrap.css" rel="stylesheet" media="screen">
  <link href="css/bootstrap-theme.css" rel="stylesheet">
HEAD;
  echo $header;
}

function addJS() {
  $js = <<< JS
<script src="js/jquery.js"></script>
<script src="js/ui/jquery-ui.js"></script>
<script src="js/bootstrap.min.js"></script>
JS;
  echo $js;
}

function semesterGPAs() {
$mysqli = new mysqli("54.218.151.84", "root", "testudo", "hardshell");
$mysqli2 = new mysqli("54.218.151.84", "root", "testudo", "hardshell");
  $gpas = Array();
  $res = $mysqli->query("SELECT DISTINCT semestername FROM student_classes WHERE username = '".phpCAS::getUser()."'"); 
  $res->data_seek(0);
    $i = 0;
  while ( $row = $res->fetch_assoc() ) {
    $res2 = $mysqli2->query("SELECT courseqp, coursecredits FROM student_classes WHERE username = '".phpCAS::getUser()."' AND semestername = '".$row['semestername']."'");
    $res2->data_seek(0);
    $totalqp = 0;
    $totalcredits = 0;
    while( $row2 = $res2->fetch_assoc() ) {
      $totalqp = $totalqp + $row2['courseqp'];
      $totalcredits = $totalcredits + $row2['coursecredits'];
    }
    if( $totalcredits != 0 ) {
    $gpas[] = "[".$i.",".$totalqp / $totalcredits."]";
    $i++;
    }

  }
  //upload page link open new tab
  ////advisor contactt

  return $gpas;
}

function cumulGPA() {
  $mysqli = new mysqli("54.218.151.84", "root", "testudo", "hardshell");
  $res = $mysqli->query("SELECT courseqp, coursecredits FROM student_classes WHERE username = '".phpCAS::getUser()."'");
  $res->data_seek(0);
  $cumuqp = 0;
  $cumucr = 0;
  while( $row = $res->fetch_assoc() ) {
    $cumuqp = $cumuqp + $row['courseqp'];
    $cumucr = $cumucr + $row['coursecredits'];
  }
  if( $cumucr != 0 ) {
  $cumu = $cumuqp / $cumucr;
  return $cumu;
  }
  return 0;
}

function perComp() {
  $mysqli = new mysqli("54.218.151.84", "root", "testudo", "hardshell");
  $res = $mysqli->query("SELECT percentcomplete FROM students WHERE username = '".phpCAS::getUser()."'");
  $res->data_seek(0);
  $row = $res->fetch_assoc();
  return $row['percentcomplete'];
}

function makeNav($name){
  $student_home = '';
  $student_degree = '';
  $student_upload = '';
  
  $advisor_home = '';
  $advisor_degree = '';
  $advisor_student = '';

  if($name == 'student_home') {
    $student_home = "class='active'";
  } elseif ($name == 'student_degree') {
    $student_degree = "class='active'";
  } elseif ($name == 'student_upload') {
    $student_upload = "class='active'";
  } elseif ($name == 'advisor_home') {
    $advisor_home = "class='active'";
  } elseif ($name == 'advisor_degree') {
    $advisor_degree = "class='active'";
  } elseif ($name == 'advisor_student') {
    $advisor_student = "class='active'";
  }


  $user = phpCAS::getUser();

  $mysqli1 = new mysqli("54.218.151.84", "root", "testudo", "hardshell");
  if( $mysqli1->connect_errno) {
        echo "Failed to connect to MySQL: (" . $mysqli1->connect_errno . ") " . $mysqli1->connect_error;
    }     
    
  if($stmt1 = $mysqli1->prepare("SELECT degree FROM `advisor` WHERE `advisor`.`username` = ?")){
    $stmt1->bind_param('s',$user);
    $stmt1->execute();
    $stmt1->bind_result($degree_id);
    $stmt1->fetch();
    
    if($degree_id != NULL){

$navbar = 
'<div class="navbar navbar-default navbar-static-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="home.php"><img class="img-rounded" style="margin:0px; padding:0px;padding-right:10px;" width="35px;" height="25px;" src="img/brand.png" />TerPlan</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li $advisor_degree  ><a href="degree.php">Manage Degree</a></li>
        <li $advisor_student ><a href="viewstudent.php">View Students</a></li>
      </ul>
      <div class="nav navbar-nav navbar-right">
      <ul class="nav navbar-nav">
        <li><a href="advisor_home.php">Home</a></li>
        <li><a href="CAS/auth.php?logout=">Logout</a></li>
      </ul>
      </div>
    </div><!-- end nav-collapse-->
  </div>
</div>';
    }else{

$navbar = 
'<div class="navbar navbar-default navbar-static-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="home.php"><img class="img-rounded" style="margin: 0px; padding: 0px; padding-right: 10px;" width="35px;" height="25px;" src="img/brand.png" />TerPlan</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li $student_degree ><a href="student_degree.php">Manage Degree</a></li>
        <li $student_upload ><a href="upload.php">Upload Transcript</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tools<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="stats.php">GPA Graph</a></li>
            <li><a href="#">Contact Advisor</a></li>
          </ul>
        </li>
      </ul>
      <div class="nav navbar-nav navbar-right">
      <ul class="nav navbar-nav">
        <li><a href="home.php">Home</a></li>
        <li><a href="CAS/auth.php?logout=">Logout</a></li>
      </ul>
      </div>
    </div>
  </div>
</div>';

    }

  }

  echo $navbar;
}
?>
