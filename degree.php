<?php
require_once 'CAS/config.php';
require_once 'CAS/CAS.php';
require_once 'CAS/init.php';
?>
<?php if( ! phpCAS::isAuthenticated() ) {
header("Location: index.php");
}
?>
<?php include("php/funcs.inc.php"); ?>
<!DOCTYPE html>
<html>
<?php addHeader("TerPlan: View Degree Creator"); ?>
<link href="css/jquery-ui.css" rel="stylesheet">
<style>
.delete-p{
	float:right;
}
.delete-h3{
	float:right;
}
</style>
</head>
<body>
 
<?php
	makeNav("adivsor_degree");
?>



<div class="container">
  <div class="row">

  <div id="course-dialog" title="Add a course">
		<label for="name">Enter a Course:</label>
		<input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" />
	</div>

	<div id="group-dialog" title="Add a group">
		<label for="name">Enter a Group:</label>
		<input type="text" name="name" id="group" class="text ui-widget-content ui-corner-all" />
	</div>
		<?php 

			$user = phpCAS::getUser();

			$mysqli1 = new mysqli("54.218.151.84", "root", "testudo", "hardshell");
			if( $mysqli1->connect_errno) {
    				echo "Failed to connect to MySQL: (" . $mysqli1->connect_errno . ") " . $mysqli1->connect_error;
  			}			
				
			if($stmt1 = $mysqli1->prepare("SELECT degree FROM `advisor` WHERE `advisor`.`username` = ?")){
				$stmt1->bind_param('s',$user);
				$stmt1->execute();
				$stmt1->bind_result($degree_id);
				$stmt1->fetch();
				
				if($degree_id != NULL){
					$cool = true;
				}else{
					$cool = false; 
				}
			
			}

			if($cool){
			
			$mysqli = new mysqli("54.218.151.84", "root", "testudo", "hardshell");
			if( $mysqli->connect_errno) {
    				echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
  			}	
				
			if($stmt = $mysqli->prepare("SELECT * FROM `degree`,`group` WHERE `degree`.`degree_id` = ? AND `degree`.`degree_id` = `group`.`degree_id` AND NOT `group`.`order` = 0")){
					
				$stmt->bind_param('i',$degree_id);
					
				if($stmt->execute()){
					
					$stmt->bind_result($degree_id,$degree_name,$group_id,$trash,$group_Name,$order);
					
					echo "<div id=\"accordion\" class=\"col-lg-10\">";
					while($stmt->fetch()){
						echo "<h3>" . $group_Name . "<button class = \"delete-h3 \">X</button></h3>";
						echo "<div>";
						printGroup($group_id);
						echo "<div>";
						echo "<button class = \"course-button btn \">Add Course</button>";
						echo "<button class = \"accordian-button btn \">Add a Group</button>";
						echo "</div>";
						echo "</div>";
					}
						echo "<div>";
						echo "<button class = \"accordian-button btn\">Add a Group</button>";
						echo "</div>";
					echo "</div>";
				} else {
					echo $stmt->error;
				}

			} else {
				echo $mysqli->error;
				echo "failed";
			}
			} else {

				echo "Not cool, bro";
			}
		
		function printGroup($groupID){
			$mysqli = new mysqli("54.218.151.84","root","testudo","hardshell");
			if( $mysqli->connect_errno) {
				echo "Falied to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
			}

			

			if($stmt = $mysqli->prepare("SELECT type,data FROM `group_entry` WHERE `group_entry`.`group_ID` = ? ORDER BY `group_entry`.`type`,`group_entry`.`order`")){
					
				$stmt->bind_param('i',$groupID);
						
				if($stmt->execute()){
					
					$stmt->bind_result($type,$data);
					$groups = false; 
					while($stmt->fetch()){
						if($type == "class"){
							echo "<p>" . $data . "<button class = \"delete-p \">X</button></p>";
						} else {
							if($groups == false){
								$mysqli2 = new mysqli("54.218.151.84","root","testudo","hardshell");
								if( $mysqli2->connect_errno) {
									echo "Falied to connect to MySQL: (" . $mysqli2->connect_errno . ") " . $mysqli2->connect_error;
								}

			
								if($stmt2 = $mysqli2->prepare("SELECT group_Name FROM `group` WHERE `group`.`group_ID` = ?")){
									$stmt2->bind_param('i',$data);
									$stmt2->execute();
									$stmt2->bind_result($group_name);
									$stmt2->fetch();
									echo "<div id=\"accordion\" class\"col-lg-10\">";
									echo "<h3>" . $group_name . "<button class = \"delete-h3 \">X</button></h3>";
									echo "<div>";
									printGroup($data);
									echo "<div>";
									echo "<button class = \"course-button btn \">Add Course</button>";
									echo "<button class = \"accordian-button btn \">Add a Group</button>";
									echo "</div>";
									echo "</div>";
								}							
								$groups = true; 
							} else {
								$mysqli1 = new mysqli("54.218.151.84","root","testudo","hardshell");
								if( $mysqli1->connect_errno) {
									echo "Falied to connect to MySQL: (" . $mysqli1->connect_errno . ") " . $mysqli1->connect_error;
								}

			
								if($stmt1 = $mysqli1->prepare("SELECT group_Name FROM `group` WHERE `group`.`group_ID` = ?")){
									$stmt1->bind_param('i',$data);
									$stmt1->execute();
									$stmt1->bind_result($group_name);
									$stmt1->fetch();
									echo "<h3>" . $group_name . "<button class = \"delete-h3 \">X</button></h3>";
									echo "<div>";
									printGroup($data);
									echo "<div>";
									echo "<button class = \"course-button btn \">Add Course</button>";
									echo "<button class = \"accordian-button btn \">Add a Group</button>";
									echo "</div>";
									echo "</div>";
								}
							}
						}
					}
					if($groups == true)
						echo "</div>";
				} else {
					echo $stmt->error;
				}

			} else {
				echo $mysqli->error;
				echo "failed";
			}

		

		}	
		?>
        </div>

  </div>

      <!-- END: main page -->

</div>

<?php addJS(); ?>

<script>
	var toUpdate; 

	$( document ).ready(function() {
		var currCount = 0; 

    	$("#accordion").accordion({
			collapsible:true,
			active:false,
			heightStyle:"content",
			beforeActivate: function(event, ui) {
         		// The accordion believes a panel is being opened
         		if (ui.newHeader[0]) {
         			var currHeader  = ui.newHeader;
         			var currContent = currHeader.next('.ui-accordion-content');
         		// The accordion believes a panel is being closed
     			} else {
     				var currHeader  = ui.oldHeader;
     				var currContent = currHeader.next('.ui-accordion-content');
     			}
         		// Since we've changed the default behavior, this detects the actual status
         		var isPanelSelected = currHeader.attr('aria-selected') == 'true';

         		// Toggle the panel's header
         		currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));

        		// Toggle the panel's icon
        		currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e',isPanelSelected).toggleClass('ui-icon-triangle-1-s',!isPanelSelected);

         		// Toggle the panel's content
         		currContent.toggleClass('accordion-content-active',!isPanelSelected)    
         		if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }

        		return false; // Cancel the default action
    			}

		});

		

		//Dialog requesting course
		$( "#course-dialog" ).dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Enter": function() {
					toUpdate.prepend("<p>"+$("#name").val()+"<button class = \"delete-p \">X</button></p>");
					toUpdate = "";
					$("#name").html('');
					$( this ).dialog( "close" );
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {
				return;
			}
		});

		//Dialog requesting grouping
		$( "#group-dialog" ).dialog({
			autoOpen: false,
			modal: true,
			buttons: {
				"Enter": function() {
					toUpdate.prepend("<div id=\"toAccord"+currCount+"\"><h3>" + $("#group").val() + "<button class = \"delete-h3 \">X</button></h3><div><div><button class = \"course-button btn \">Add Course</button><button class = \"accordian-button btn \">Add a Group</button></div></div></div>");
					
					$("#toAccord"+currCount).accordion({
						collapsible:true,
						heightStyle:"content",
						beforeActivate: function(event, ui) {
			         		// The accordion believes a panel is being opened
			         		if (ui.newHeader[0]) {
			         			var currHeader  = ui.newHeader;
			         			var currContent = currHeader.next('.ui-accordion-content');
			         		// The accordion believes a panel is being closed
			     			} else {
			     				var currHeader  = ui.oldHeader;
			     				var currContent = currHeader.next('.ui-accordion-content');
			     			}
			         		// Since we've changed the default behavior, this detects the actual status
			         		var isPanelSelected = currHeader.attr('aria-selected') == 'true';

			         		// Toggle the panel's header
			         		currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));

			        		// Toggle the panel's icon
			        		currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e',isPanelSelected).toggleClass('ui-icon-triangle-1-s',!isPanelSelected);

			         		// Toggle the panel's content
			         		currContent.toggleClass('accordion-content-active',!isPanelSelected)    
			         		if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }

			        		return false; // Cancel the default action
			    			}

					});

					currCount++;
					toUpdate = "";
					$("#group").html('');
					$( this ).dialog( "close" );
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {
			}
		});

	});

	$(document).on("click",".delete-p",function() {
		console.log($(this).parent());
		$(this).closest('p').remove();
	});

	$(document).on("click",".delete-h3",function() {
		console.log($(this).nextUntil("h3"));
		$(this).parent().nextUntil("h3").remove();
		$(this).closest('h3').remove();
	});

	$(document).on("click",".accordian-button",function() {
		toUpdate = $(this).parent().parent();
		$( "#group-dialog" ).dialog( "open" );
	});

	$(document).on("click",".course-button",function() {
		console.log($(this));
		toUpdate = $(this).parent().parent();
		$( "#course-dialog" ).dialog( "open" );
	});

 	</script>
  </body>
</html>
