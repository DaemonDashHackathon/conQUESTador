<?php
require_once 'CAS/config.php';
require_once 'CAS/CAS.php';
require_once 'CAS/init.php';
?>
<?php if( ! phpCAS::isAuthenticated() ) {
header("Location: index.php");
}

$user = phpCAS::getUser();

 

  $mysqli2 = new mysqli("54.218.151.84", "root", "testudo", "hardshell");
  if( $mysqli2->connect_errno) {
            echo "Failed to connect to MySQL: (" . $mysqli2->connect_errno . ") " . $mysqli2->connect_error;
        }     
        
      if($stmt2 = $mysqli2->prepare("SELECT degree FROM `advisor` WHERE `advisor`.`username` = ?")){
        $stmt2->bind_param('s',$user);
        $stmt2->execute();
        $stmt2->bind_result($degree_id);
        $stmt2->fetch();
        
        if($degree_id != NULL){
          header("Location: advisor_home.php");
        }
      
  }
 
 $mysqli1 = new mysqli("54.218.151.84", "root", "testudo", "hardshell");
if( $mysqli1->connect_errno) {
      echo "Failed to connect to MySQL: (" . $mysqli1->connect_errno . ") " . $mysqli1->connect_error;
  }     

if($stmt1 = $mysqli1->prepare("SELECT degree_id FROM `students` WHERE `students`.`username` = ?")){
  $stmt1->bind_param('s',$user);
  $stmt1->execute();
  $stmt1->bind_result($degree_id);
  $stmt1->fetch();

}

if(!isset($degree_id) || $degree_id == 0){
  $hide = true; 
} else {
  $hide = false; 
}

//check if advisor, redirect to appropriate home page?
?>
<?php include("php/funcs.inc.php"); ?>
<!DOCTYPE html>
<html>
<?php addHeader("TerPlan: Home"); ?>
<link href="css/justified-nav.css" rel="stylesheet">
<link href="js/jqplot/jquery.jqplot.css" rel="stylesheet">
</head>
<body style="padding-top:0px;">

<?php makeNav("home"); ?>

<div class="container">
  <div class="jumbotron">
    <h1>TerPlan</h1>
<?php if($hide) { ?>
    <p class="lead">It looks like you haven't yet uploaded your transcript.  Click below to do so and we can get started showing you all kinds of useful information!</p>
<?php } else { ?>
    <p class="lead">Is it time to update your transcript yet?</p>
<?php } ?>
    <p><a class="btn btn-lg btn-success" href="upload.php">Upload It</a></p>
  </div>

  <div class="row" <?php if($hide){ echo "style=\"display:none\""; } ?>>
    <div class="col-lg-4">
      <h2>Degree Progress</h2>
<?php $per = perComp(); ?>
<p class="lead">You're <strong>48%</strong> completed with your degree!</p>
      <div class="progress progress-striped">
      <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="48" aria-valuemin="0" aria-valuemax="100" style="width: 48%"><span class="sr-only">48% Complete</span></div>
      </div>
      <p><a class="btn btn-primary" href="student_degree.php">View Degree Plan &raquo;</a></p>
    </div>
    <div class="col-lg-4">
      <h2>GPA Stats</h2>
      <p class="lead">Your GPA is trending <strong>down</strong>. Study harder!</p>
      <div style="height: 250px;width: 375px;" id="chartdiv"></div>
      <p><a class="btn btn-primary" href="stats.php">More Details &raquo;</a></p>
    </div>
    <div class="col-lg-4">
      <h2>Advisor Meeting?</h2>
      <p class="lead">We now support advisor degree reviews. Easily review your degree completion with your advisors!</p>
      <p><a class="btn btn-primary" href="#">Lets Go &raquo;</a></p>
    </div>
</div>


<?php addJS(); ?>
<script type="text/javascript" src="js/jqplot/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="js/jqplot/plugins/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="js/jqplot/plugins/jqplot.trendline.min.js"></script>
<script>
$(document).ready(function() {
  $.jqplot.config.enablePlugins = true;
  $.jqplot('chartdiv', [[<?php echo join(",", semesterGPAs()); ?>]], {
    animate: true,
    animateReplot: true,
    axes: {
      xaxis: {
        showTicks: false,
        numberTicks: <?php echo sizeof(semesterGPAs())/2; ?>,
      },
      yaxis: {
        showTicks: true,
      },
    },
    highlighter: { show: true, }
  });
});
</script>
</body>
</html>
