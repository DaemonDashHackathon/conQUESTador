<?php
require_once 'CAS/config.php';
require_once 'CAS/CAS.php';
require_once 'CAS/init.php';
?>
<?php if( ! phpCAS::isAuthenticated() ) {
header("Location: index.php");
}
?>
<?php include("php/funcs.inc.php"); ?>
<!DOCTYPE html>
<html>
<?php addHeader("TerPlan: GPA"); ?>
<link href="css/justified-nav.css" rel="stylesheet">
<link href="js/jqplot/jquery.jqplot.css" rel="stylesheet">
</head>
<body style="padding-top:0px;">

<?php makeNav("stats"); ?>

<div class="container">
  <div class="row">
    <div class="col-lg-offset-3 col-lg-6">
      <h1>GPA Information</h1>
<?php
$cumu = cumulGPA();
$grade = "A";
if( $cumu > 3.75 ) {
  $grade = "A";
} else if ( $cumu > 3.25 ) {
  $grade = "A-";
} else if ( $cumu > 3 ) {
  $grade = "B+";
} else if ( $cumu > 2.75 ) {
  $grade = "B";
} else if ( $cumu > 2.25 ) {
  $grade = "B-";
}
?>
      <p class="lead">You cumulative GPA is currently <strong><?php echo round($cumu, 2); ?></strong>.</p>
      <p>Your GPA is trending <strong>down</strong>, study harder! A(n) <strong><?php echo $grade; ?></strong> average this semester would help raise your cumulative GPA.</p>
      <div style="height: 500px;width: 600px;" id="chartdiv"></div>
    </div>
</div>

<?php addJS(); ?>
<script type="text/javascript" src="js/jqplot/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="js/jqplot/plugins/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="js/jqplot/plugins/jqplot.trendline.min.js"></script>
<script>
$(document).ready(function() {
  $.jqplot.config.enablePlugins = true;
  $.jqplot('chartdiv', [[<?php echo join(",", semesterGPAs()); ?>]], {
    animate: true,
    animateReplot: true,
    axes: {
      xaxis: {
        showTicks: false,
        numberTicks: <?php echo sizeof(semesterGPAs())/2; ?>,
      },
      yaxis: {
        showTicks: true,
      },
    },
    highlighter: { show: true, }
  });
});
</script>
</body>
</html>
